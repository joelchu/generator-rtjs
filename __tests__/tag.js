'use strict';
const path = require('path');
const assert = require('yeoman-assert');
const helpers = require('yeoman-test');

const config = require('../lib/config.json');
const ext = 'tag';
const join = path.join;
const tagName = 'my-new-tag';

describe('generator-rtjs:tag', () => {
  it('creates tag files', () => {
    helpers.run(
            join(__dirname, '../generators/tag')
        ).withArguments([
          tagName
        ]).then(() => {
          assert.file([
            join(config.appPath, 'scripts', 'components', tagName, [tagName, ext].join('.')),
            join(config.testPath, 'tags', [tagName, 'test', ext].join('.'))
          ]);
        });
  });
});
