'use strict';
const path = require('path');
const assert = require('yeoman-assert');
const helpers = require('yeoman-test');

describe('generator-rtjs:app', () => {
  it('creates package.json and bower.json', () => {
    helpers.run(
            path.join(__dirname, '../generators/app')
        ).withOptions({
          lang: 'en'
        }).withArguments([
          'my-test-app'
        ]).withPrompts({
          useGit: true
        }).withPrompts({
          cssFramework: 'none'
        }).withPrompts({
          installer: 'npm'
        }).then(() => {
          assert.file([
            'package.json', 'bower.json'
          ]);
        });
  });
});
