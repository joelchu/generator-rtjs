'use strict';
const path = require('path');
const assert = require('yeoman-assert');
const helpers = require('yeoman-test');

const config = require('../lib/config.json');
const join = path.join;
const serviceName = 'my-test-srv';
const ext = 'js';
// Const fs = require('fs-extra');

describe('generator-rtjs:service', () => {
  it('creates service files', () => {
    helpers.run(
            join(__dirname, '../generators/service')
        ).withArguments([
          serviceName
        ]).withOptions({
          'skip-import': 1
        }).then(() => {
          assert.file([
            join(config.appPath, 'scripts', 'services', [serviceName, ext].join('.')),
            join(config.testPath, 'services', [serviceName, 'test', ext].join('.'))
          ]);
        });
  });
});
