'use strict';

const BaseClass = require('../../lib/base-class.js');
const join = require('path').join;

module.exports = class extends BaseClass {

  constructor(args, opts) {
    super(args, opts);
    this.ext = 'js';
    this._loadLangFile(this.config.get('lang'));
        // Get the tag name
    if (args[0]) {
      const name = args[0];

      this.mixinName = this._toCamel(name);
      this.mixinFileName = this._camelToDash(name);
            // Get if there is a c / component flag
      this._checkComponent(opts);
    } else {
      throw this.langObj.ERR_MISSING_MIXIN_NAME;
    }
  }

  /**
   * Generate the mixin file
   */
  _writeMixinFile() {
    const fileName = [this.mixinFileName, this.ext].join('.');
    const mixinFilePath = (this.componentDir) ? join(this.componentDir, 'mixins', fileName) : join(this.mixinPath, fileName);
    const params = {
      mixinName: this.mixinName,
      mixinPath: mixinFilePath,
      mixinFileName: this.mixinFileName
    };
    this._copyTpl('minxin.txt', mixinFilePath, params);
  }
  /**
   * Generate the mixin test file
   */
  _writeMixinTest() {
    const fileName = [this.mixinFileName, 'test', this.ext].join('.');
    const mixinTestPath = (this.componentTestDir) ? join(this.componentTestDir, 'mixins', fileName) : join(this.testPath, 'mixins', fileName);
    const params = {
      mixinName: this.mixinName,
      mixinPath: mixinTestPath,
      mixinFileName: this.mixinFileName
    };
    this._copyTpl('mixin-test.txt', mixinTestPath, params);
  }

  /**
   * Execute the write
   */
  writing() {
    this._writeMixinFile();
    this._writeMixinTest();
  }
};
