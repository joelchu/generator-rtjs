'use strict';

const BaseClass = require('../../lib/base-class.js');
const join = require('path').join;

module.exports = class extends BaseClass {

  constructor(args, opts) {
    super(args, opts);
    this.ext = 'tag';
    this._loadLangFile(this.config.get('lang'));

        // Get the tag name
    if (args[0]) {
      this.tagName = args[0];

      this.isStateless = opts.stateless !== undefined;

            // Get if there is a c / component flag
      this._checkComponent(opts);
    } else {
      throw this.langObj.ERR_MISSING_TAG_NAME;
    }
  }
  /**
   * Create the tag file
   */
  _writeTagFile() {
    const tagName = this._camelToDash(this.tagName);
    const fileName = [tagName, this.ext].join('.');

    const filePath = (this.componentDir) ? join(this.componentDir, fileName) : join(this.tagPath, tagName, fileName);
    const params = {
      tagName: tagName,
      isStateless: this.isStateless
    };
    this._copyTpl('tag.txt', filePath, params);
  }
  /**
   * Generate the test file based on the template
   */
  _writeTagTestFile() {
    const tagName = this._camelToDash(this.tagName);
    const fileName = [tagName, 'test', 'js'].join('.');
    const filePath = (this.componentTestDir) ? join(this.componentTestDir, 'tags', fileName) : join(this.testPath, 'tags', fileName);
    const params = {
      tagName: tagName,
      isStateless: this.isStateless,
      filePath: filePath
    };
    this._copyTpl('tag-test.txt', filePath, params);
  }

  /**
   * Finally execute the write file
   */
  writing() {
    this._writeTagFile();
    this._writeTagTestFile();
      // @TODO search for the existing top level component by name
      // then inject the import statement into that file
  }

};
